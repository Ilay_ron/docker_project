docker image

* pull container name nginx
* inspect the container a d write down the hostname given to the container

docker container

* list all runningcontainers on your system
* run a container of busybox instance
* check if it is running
if not inspect why it is not running
* create another container based on nginx, but run it in deamon mode
* inspect the contatner and write down the ip of it
* validate the nginx connection with curl or wget
* check logs of the container
* attach your shell to nginx and check if you have stdin/out/err
* if nothing happening exit the container
* check that container is still running, no? why?
* start container with id
* check stats new started container
* exec /bin/bash command on container interactively
